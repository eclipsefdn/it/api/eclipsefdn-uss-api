# eclipsefdn-uss-api

This API manages the USS operations.

## Deprecation Notice

**Important:** The Eclipse User Storage Service (USS) API will be taken offline on **July 2, 2025**. After this date, the service will no longer be supported or maintained. Projects are encouraged to transition to alternative solutions as soon as possible. For more information, please refer to our [GitLab Helpdesk Issue #4696](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/4696).

## Getting started

### Requirements

* Docker
* mvn
* make
* yarn
* Java 11 >

### Setup

Additional setup instructions will be added as the project develops.

#### Build and Start Server

```bash
make compile-start
```

#### Live-coding Dev Mode

```bash
make dev-start
```

#### Generate Spec

```bash
make compile-test-resources
```

#### Running Tests

```bash
mvn test
```

#### Render a Live UI Preview of the API Spec

```bash
make start-spec
```

## Testing with the Eclipse IDE

### Updating the Service URI

*NOTE: This step can be skipped if testing with prod*

By default, the Eclipse IDE will use the production URL of this API. To test with the staging/local instance of this API, the IDE must first be configured to allow this.

* Locate your Eclipse IDE install directory
* Open and edit the **eclipse.ini** file
* Under the `-vmargs` section, add the following argument:
* `-Dorg.eclipse.userstorage.serviceURI={URL}`, where `{URL}` is the specific URL you wish to use. E.g. `-Dorg.eclipse.userstorage.serviceURI=https://api-staging.eclipse.org`
  * Staging URL: `https://api-staging.eclipse.org`
  * Local Docker container URL: `localhost:10129`
  * Local in dev-mode: `localhost:8090`

#### Confirmation

There is no easy way to view that the IDE is connected to your staging/local instance aside from basic DB confirmation.

* You can take a look at the staging/local DB to view all `blob_application_usage` records that were created while performing the read/write operations
  * You can filter these entries by UID
* You should be able to see an entry with your UID in the `user_blob` table with an updated `changed` field

### Authorize USS with your Eclipse Account (if this hasn’t already been done)

* Use **Window** > **Preferences** > **Oomph** > **Setup Tasks** > **Preference Synchronizer**
* Check the **Synchronize with eclipse.org** box
* If it does not automatically prompt you for a sign-in, click **Synchronize Now**
* Sign in to accounts
* Authorize the request
  * You can view/revoke this access in your Eclipse profile page at `https://accounts.eclipse.org/users/{ef-username}/storage` under the **applications** tab, where `{ef-username}` is your EF account username.

### To Sync Settings

* Use **Navigate** > **Open Setup** > **User** and click on the **Capture Preferences** toolbar button.
* Add any desired settings from the left side to the right side
* Click **OK**
  * The main *user.setup* panel should display all user preferences being synced
* Click the **Record Preferences** toolbar button
  * This should reopen the **Preference Synchronizer** menu
  * **OR** Use **Window** > **Preferences** > **Oomph** > **Setup Tasks** > **Preference Synchronizer**
* Click **Synchronize Now**
* You can review the updates and change whether you want any preferences to not get synced with eclipse.org
* Click **OK**
  * Your settings should now be synced

### To View Synced Settings

* Click the **Record Preferences** toolbar button
  * This should reopen the **Preference Synchronizer** menu
  * **OR** Use **Window** > **Preferences** > **Oomph** > **Setup Tasks** > **Preference Synchronizer**
* Click **View Remote Storage** to view all synced settings

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-uss-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-uss-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-uss-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
<http://www.eclipse.org/legal/epl-2.0>.

SPDX-License-Identifier: EPL-2.0
