/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.request;

import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.eclipsefoundation.http.config.OAuth2SecurityConfig;
import org.eclipsefoundation.uss.config.OAuthFilterConfig;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.UnauthorizedException;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;

/**
 * This filter is used to validate the incoming Bearer tokens. Sets the current token user in the request context for use downstream.
 */
public class OAuthFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthFilter.class);

    @Inject
    Instance<OAuthFilterConfig> config;
    @Inject
    OAuth2SecurityConfig config2;

    @Inject
    AuthenticatedRequestWrapper token;

    @ServerRequestFilter(priority = Priorities.USER - 11)
    public void filterByAuthentication(ContainerRequestContext requestContext) {
        if (config.get().enabled()) {
            try {
                LOGGER.debug("All passed headers: {}", requestContext.getHeaders());
                var tokenStatus = token.getTokenStatus();
                if (tokenStatus == null || tokenStatus.userId() == null) {
                    LOGGER.error("User cannot be anonymous");
                    throw new UnauthorizedException("User cannot be anonymous");
                }

                // check that the proper scopes are set in the
                if (!DrupalAuthHelper
                        .hasScopes(tokenStatus.scope(),
                                requestContext.getMethod().equalsIgnoreCase(HttpMethod.GET) ? config.get().allowedReadScopes()
                                        : config.get().allowedWriteScopes())) {
                    throw new UnauthorizedException("This token has invalid scope(s)");
                }
            } catch (FinalForbiddenException e) {
                throw new UnauthorizedException("Invalid credentials", e);
            }
        }
    }
}
