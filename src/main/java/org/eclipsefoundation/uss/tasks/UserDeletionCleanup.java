/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.tasks;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.uss.api.UserDeleteAPI;
import org.eclipsefoundation.uss.api.models.UserDeleteRequest;
import org.eclipsefoundation.uss.api.models.UserDeleteRequestResponse;
import org.eclipsefoundation.uss.config.CleanupTaskConfig;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.dtos.USSApplicationUsage;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@ApplicationScoped
public class UserDeletionCleanup {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDeletionCleanup.class);

    // We're unlikely to hit 100 results. This helps prevent extra requests
    private static final int LIMIT = 100;
    private static final int DELETE_REQ_STATUS = 0;

    @Inject
    CleanupTaskConfig config;

    @Inject
    @RestClient
    UserDeleteAPI api;
    @Inject
    DrupalTokenService tokenService;
    @Inject
    FilterService filters;
    @Inject
    PersistenceDao dao;

    @Scheduled(every = "PT1H", delay = 10, delayUnit = TimeUnit.SECONDS)
    public void performBlobCleanup() {

        if (!config.enabled()) {
            LOGGER.info("Blob cleanup task disabled thorugh configuration.");
        } else {
            String bearerToken = "Bearer " + tokenService.getToken();

            List<UserDeleteRequest> deletionRequests = getAllDeletionRequests(bearerToken);
            LOGGER.info("Blob Cleanup - Found {} deletion requests", deletionRequests.size());

            RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org/uss"));

            // Delete Blob records for each user and send a request to delete the deletion request
            deletionRequests.stream().forEach(req -> {
                MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
                params.add(UssAPIParameterNames.UID.getName(), req.uid());
                dao.delete(new RDBMSQuery<>(wrap, filters.get(Blob.class), params));
                LOGGER.info("Blob Cleanup - Removed Blob for user {}:{}", req.uid(), req.name());

                // Clean up usage records for user
                dao.delete(new RDBMSQuery<>(wrap, filters.get(USSApplicationUsage.class), params));
                LOGGER.info("Blob Cleanup - Removed usage records for user {}:{}", req.uid(), req.name());

                Response deletionResp = api.completeDeleteRequest(bearerToken, req.id());
                if (deletionResp.getStatus() != Status.NO_CONTENT.getStatusCode()) {
                    LOGGER.warn("Error deleting 'delete_request' with id: {}, manual deletion may be required", req.id());
                } else {
                    LOGGER.info("Blob Cleanup - Removed 'delete_request' {} for user {}:{}", req.id(), req.uid(), req.name());
                }
            });
        }
    }

    /**
     * Using pagination, we iterate over all results from the user delete api. We do this manually since the return object is a bit
     * unconventional and is not compatible with our APIMiddleware service.
     * 
     * @param bearerToken The generated Oauth2 token header
     * @return All UserDeleteRequest entities bound to the uss host.
     */
    private List<UserDeleteRequest> getAllDeletionRequests(String bearerToken) {
        int currentPage = 1;
        BaseAPIParameters pagination = new BaseAPIParameters(currentPage, LIMIT, null);

        Response response = api.getDeleteRequests(pagination, bearerToken, DELETE_REQ_STATUS, config.host());

        UserDeleteRequestResponse respBody = response.readEntity(UserDeleteRequestResponse.class);
        List<UserDeleteRequest> out = respBody.result();

        // Iterate over each page of results, adding all to outbound list
        while (response.hasLink("next")) {
            pagination = new BaseAPIParameters(++currentPage, LIMIT, null);
            response = api.getDeleteRequests(pagination, bearerToken, DELETE_REQ_STATUS, config.host());
            respBody = response.readEntity(UserDeleteRequestResponse.class);
            out.addAll(respBody.result());
        }

        return out;
    }
}
