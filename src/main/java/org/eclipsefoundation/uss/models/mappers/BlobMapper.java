/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.models.mappers;

import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.models.BlobData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for converting from Blob DTO to Blob model class. Uses java
 * expressions to map DTO blobValue field from byte[] to String, and for
 * constructing a custom URL using the DTO uid, applicationToken, and blobKey fields.
 */
@Mapper(config = QuarkusMappingConfig.class)
public interface BlobMapper extends BaseEntityMapper<Blob, BlobData> {

    @Mapping(source = "blobKey", target = "key")
    @Mapping(target = "value", expression = "java(new String(dto.getBlobValue()))")
    @Mapping(target = "url", expression = "java(String.format(\"https://api.eclipse.org/account/profile/%d/blob/%s/%s\", dto.getUid(), dto.getApplicationToken(), dto.getBlobKey()))")
    BlobData toModel(Blob dto);
}
