/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Incoming request body for creating/updating a Blob.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_BlobUpdateRequest.Builder.class)
public abstract class BlobUpdateRequest {

    public abstract String getValue();

    public static Builder builder() {
        return new AutoValue_BlobUpdateRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setValue(String value);

        public abstract BlobUpdateRequest build();
    }
}