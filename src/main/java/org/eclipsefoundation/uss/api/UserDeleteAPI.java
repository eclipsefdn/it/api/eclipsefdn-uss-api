/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * User deletion API binding. Allows for fetching all deletion requests for a specific host, as well as deleting a specific request.
 */
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "user-delete-api")
public interface UserDeleteAPI {

    @GET
    Response getDeleteRequests(@BeanParam BaseAPIParameters pagination, @HeaderParam(HttpHeaders.AUTHORIZATION) String token,
            @QueryParam("status") int status, @QueryParam("host") String host);

    @DELETE
    @Path("{requestId}")
    Response completeDeleteRequest(@HeaderParam(HttpHeaders.AUTHORIZATION) String token, @PathParam("requestId") String requestId);
}
