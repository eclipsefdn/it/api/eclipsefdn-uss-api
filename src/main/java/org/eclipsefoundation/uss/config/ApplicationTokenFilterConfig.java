/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.config;

import java.util.Map;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Loads all ApplicationTokenFilter properties. Contains an enabled
 * flag, as well as a list of allowed application tokens. These configs can be
 * modified by updating the '/main/resources/application.properties' or
 * '/test/resources/application.properties' files.
 */
@ConfigMapping(prefix = "eclipse.uss.app-tokens-filter")
public interface ApplicationTokenFilterConfig {

    @WithDefault(value = "true")
    boolean enabled();

    Map<String, String> allowedTokens();
}
