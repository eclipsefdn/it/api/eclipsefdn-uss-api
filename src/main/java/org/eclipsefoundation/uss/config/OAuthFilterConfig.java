/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Loads all OauthFilter config properties. Includes an enabled flag, a list of
 * allowed read/write scopes, and a list of allowed client ids.
 * 
 * The scopes and client ids are configured by updating the
 * '/config/application/secret.properties' file.
 * The enabled flag can be configured via the
 * '/main/resources/application.properties' file.
 * For testing, all need mocked/stubbed values via the
 * '/test/resources/application.properties' file.
 */
@ConfigMapping(prefix = "eclipse.uss.oauth-filter")
public interface OAuthFilterConfig {

    @WithDefault(value = "true")
    boolean enabled();

    @WithDefault(value = "uss_retrieve")
    List<String> allowedReadScopes();

    @WithDefault(value = "uss_update")
    List<String> allowedWriteScopes();

    List<String> allowedClients();
}
