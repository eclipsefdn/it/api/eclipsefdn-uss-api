
-- eclipse_api.blob

CREATE TABLE `user_blob` (
  `bid` SERIAL NOT NULL,
  `uid` int NOT NULL DEFAULT 0,
  `blob_key` varchar(255) NOT NULL,
  `application_token` varchar(255) NOT NULL,
  `blob_value` BLOB NOT NULL,
  `etag` varchar(255) NOT NULL,
  `created` int NOT NULL DEFAULT 0,
  `changed` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`bid`)
);

INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'testKey', 'testToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'testTag', 1684509568, 1684509568);
INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'filename.txt', 'testToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'etag', 1684509568, 1684509568);
INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'eclipseKey', 'eclipseToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'eTaG30Test', 1684509568, 1684509568);
INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'application.properties', 'eclipseToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'appTagTest', 1684509568, 1684509568);
INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (1234, 'filename.txt', 'testToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'etag', 1684509568, 1684509568);
INSERT INTO `user_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (99, 'eclipseKey', 'eclipseToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'eTaG30Test', 1684509568, 1684509568);

-- eclipse_api.blob_application_usage

CREATE TABLE `blob_application_usage` (
  `id` SERIAL NOT NULL,
  `uid` int NOT NULL DEFAULT 0,
  `blob_key` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `application_token` varchar(255) NOT NULL,
  `created` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (1234, 'filename.txt', 'index', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (1234, 'filename.txt', 'retrieve', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (1234, 'filename.txt', 'create', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (1234, 'filename.txt', 'retrieve', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (1234, 'filename.txt', 'update', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (99, 'eclipseKey', 'index', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);
INSERT INTO `blob_application_usage` (`uid`, `blob_key`, `action`, `application_token`, `created`)
  VALUES (99, 'eclipseKey', 'retrieve', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 1684509568);