/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.test.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.DrupalOAuthAPI;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthDataBuilder;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;

import io.quarkus.logging.Log;
import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockDrupalOAuthAPI implements DrupalOAuthAPI {

    List<DrupalOAuthData> tokens;
    List<DrupalUserInfo> users;

    public MockDrupalOAuthAPI() {
        tokens = new ArrayList<>();
        tokens
                .addAll(Arrays
                        .asList(DrupalOAuthDataBuilder
                                .builder()
                                .accessToken("validRead")
                                .clientId("test-id")
                                .userId("42")
                                .expires(Instant.now().getEpochSecond() + 20000)
                                .scope("read")
                                .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("validWrite")
                                        .clientId("test-id")
                                        .userId("42")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("write")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("invalidScope")
                                        .clientId("test-id")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("blob")
                                        .build()));

        users = new ArrayList<>();
        users.addAll(Arrays.asList(new DrupalUserInfo("42", "fakeuser", "fakeuser"), new DrupalUserInfo("333", "otheruser", "other")));
    }

    @Override
    public DrupalOAuthData getTokenInfo(String token) {
        Log.infov("Checking token {0} for match in {1}", token, tokens);
        return tokens.stream().filter(t -> t.accessToken().equalsIgnoreCase(token)).findFirst().orElse(null);
    }

    @Override
    public DrupalUserInfo getUserInfoFromToken(String token) {
        DrupalOAuthData tokenInfo = getTokenInfo(DrupalAuthHelper.stripBearerToken(token));
        if (tokenInfo == null || tokenInfo.userId() == null) {
            throw new FinalForbiddenException("The access token provided is invalid");
        }
        Log.infov("Checking token {0} for match in {1}", token, users);
        return users.stream().filter(u -> u.sub().equalsIgnoreCase(tokenInfo.userId())).findFirst().orElse(null);
    }
}