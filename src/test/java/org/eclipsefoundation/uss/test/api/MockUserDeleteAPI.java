package org.eclipsefoundation.uss.test.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.uss.api.UserDeleteAPI;
import org.eclipsefoundation.uss.api.models.UserDeletePagination;
import org.eclipsefoundation.uss.api.models.UserDeleteRequest;
import org.eclipsefoundation.uss.api.models.UserDeleteRequestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Mock
@RestClient
@ApplicationScoped
public class MockUserDeleteAPI implements UserDeleteAPI {

    private int count;
    private List<UserDeleteRequest> deleteRequests;

    public MockUserDeleteAPI() {
        count = 0;
        deleteRequests = new ArrayList<>();
        deleteRequests
                .addAll(Arrays
                        .asList(generateMockRequest("42", "accounts.eclipse.org"), generateMockRequest("99", "api.eclipse.org/uss"),
                                generateMockRequest("99", "projects.eclipse.org"), generateMockRequest("1234", "api.eclipse.org/uss")));
    }

    @Override
    public Response getDeleteRequests(BaseAPIParameters pagination, String token, int status, String host) {
        List<UserDeleteRequest> results = deleteRequests
                .stream()
                .filter(req -> Integer.parseInt(req.status()) == status && req.host().equalsIgnoreCase(host))
                .toList();
        return Response
                .ok(new UserDeleteRequestResponse(results,
                        new UserDeletePagination(pagination.page, pagination.limit, 1, count, count, Integer.toString(count))))
                .build();
    }

    @Override
    public Response completeDeleteRequest(String token, String requestId) {
        if (deleteRequests.removeIf(req -> req.id().equals(requestId))) {
            return Response.status(Status.NO_CONTENT).build();
        }
        return Response.status(Status.NOT_FOUND).build();
    }

    private UserDeleteRequest generateMockRequest(String uid, String host) {
        String id = Integer.toString(++count);
        String name = "user" + uid;
        String mail = uid + "@email.com";
        String created = Long.toString(Instant.now().toEpochMilli());
        String changed = created;
        String url = "https://api.eclipse.org/account/user_delete_request/" + id;
        return new UserDeleteRequest(id, uid, name, mail, host, "0", created, changed, url);
    }
}
