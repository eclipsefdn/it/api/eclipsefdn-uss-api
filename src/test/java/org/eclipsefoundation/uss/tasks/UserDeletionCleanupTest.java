/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.tasks;

import java.net.URI;
import java.util.List;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.dtos.USSApplicationUsage;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class UserDeletionCleanupTest {

    @Inject
    FilterService filters;
    @Inject
    PersistenceDao dao;

    @Inject
    UserDeletionCleanup cleanupTask;

    @Test
    void performcleanup_success() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org/uss"));

        // These users should have blobs before cleanup
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(UssAPIParameterNames.UID.getName(), "1234");
        List<Blob> results = dao.get(new RDBMSQuery<>(wrap, filters.get(Blob.class), params));
        List<USSApplicationUsage> usageResults = dao.get(new RDBMSQuery<>(wrap, filters.get(USSApplicationUsage.class), params));
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertFalse(usageResults.isEmpty());

        params = new MultivaluedHashMap<>();
        params.add(UssAPIParameterNames.UID.getName(), "99");
        results = dao.get(new RDBMSQuery<>(wrap, filters.get(Blob.class), params));
        usageResults = dao.get(new RDBMSQuery<>(wrap, filters.get(USSApplicationUsage.class), params));
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertFalse(usageResults.isEmpty());

        cleanupTask.performBlobCleanup();

        // These same records should be gone after cleanup
        params = new MultivaluedHashMap<>();
        params.add(UssAPIParameterNames.UID.getName(), "1234");
        results = dao.get(new RDBMSQuery<>(wrap, filters.get(Blob.class), params));
        usageResults = dao.get(new RDBMSQuery<>(wrap, filters.get(USSApplicationUsage.class), params));
        Assertions.assertTrue(results.isEmpty());
        Assertions.assertTrue(usageResults.isEmpty());

        params = new MultivaluedHashMap<>();
        params.add(UssAPIParameterNames.UID.getName(), "99");
        results = dao.get(new RDBMSQuery<>(wrap, filters.get(Blob.class), params));
        usageResults = dao.get(new RDBMSQuery<>(wrap, filters.get(USSApplicationUsage.class), params));
        Assertions.assertTrue(results.isEmpty());
        Assertions.assertTrue(usageResults.isEmpty());
    }
}
